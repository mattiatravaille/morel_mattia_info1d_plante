-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUETES MYSQL
-- Database: zzz_xxxxx_NOM_PRENOM_INFO1X_SUJET_104_2021

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS project_btw;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS project_btw;

USE project_btw;
--
-- Base de données :  `project_btw`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_date`
--

CREATE TABLE `t_date` (
  `id_date` int(11) NOT NULL,
  `plantation_date` date NOT NULL,
  `floraison_date` date NOT NULL,
  `info_date` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_date`
--

INSERT INTO `t_date` (`id_date`, `plantation_date`, `floraison_date`, `info_date`) VALUES
(1, '2021-04-04', '2021-08-27', 'ok'),
(2, '2021-01-11', '2021-07-14', 'magique'),
(3, '2021-04-04', '2021-07-16', 'Lol');

-- --------------------------------------------------------

--
-- Structure de la table `t_date_plante`
--

CREATE TABLE `t_date_plante` (
  `id_date_plante` int(11) NOT NULL,
  `fk_date` int(11) NOT NULL,
  `fk_plante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_plante`
--

CREATE TABLE `t_plante` (
  `id_plante` int(11) NOT NULL,
  `nom_plante` varchar(32) CHARACTER SET utf8 NOT NULL,
  `famille_plante` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `position_plante` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `note_plante` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_plante`
--

INSERT INTO `t_plante` (`id_plante`, `nom_plante`, `famille_plante`, `position_plante`, `note_plante`) VALUES
(2, 'epice', 'suffoquaé', 'D4', 'Très belle'),
(3, 'Pisenlit', NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_date`
--
ALTER TABLE `t_date`
  ADD PRIMARY KEY (`id_date`);

--
-- Index pour la table `t_date_plante`
--
ALTER TABLE `t_date_plante`
  ADD PRIMARY KEY (`id_date_plante`),
  ADD KEY `fk_date` (`fk_date`),
  ADD KEY `fk_plante` (`fk_plante`),
  ADD KEY `fk_date_2` (`fk_date`,`fk_plante`),
  ADD KEY `fk_date_3` (`fk_date`),
  ADD KEY `fk_plante_2` (`fk_plante`),
  ADD KEY `fk_date_4` (`fk_date`,`fk_plante`);

--
-- Index pour la table `t_plante`
--
ALTER TABLE `t_plante`
  ADD PRIMARY KEY (`id_plante`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_date`
--
ALTER TABLE `t_date`
  MODIFY `id_date` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_date_plante`
--
ALTER TABLE `t_date_plante`
  MODIFY `id_date_plante` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_plante`
--
ALTER TABLE `t_plante`
  MODIFY `id_plante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_date_plante`
--
ALTER TABLE `t_date_plante`
  ADD CONSTRAINT `t_date_plante_ibfk_2` FOREIGN KEY (`fk_date`) REFERENCES `t_date` (`id_date`),
  ADD CONSTRAINT `t_date_plante_ibfk_3` FOREIGN KEY (`fk_plante`) REFERENCES `t_plante` (`id_plante`);