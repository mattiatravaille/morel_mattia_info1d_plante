"""
    Fichier : gestion_genres_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les dates.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from APP_FILMS import obj_mon_application
from APP_FILMS.database.connect_db_context_manager import MaBaseDeDonnee
from APP_FILMS.erreurs.exceptions import *
from APP_FILMS.erreurs.msg_erreurs import *
from APP_FILMS.date.gestion_date_wtf_forms import FormWTFAjouterdates
from APP_FILMS.date.gestion_date_wtf_forms import FormWTFDeletedate
from APP_FILMS.date.gestion_date_wtf_forms import FormWTFUpdatedate

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /dates_afficher
    
    Test : ex : http://127.0.0.1:5005/dates_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_date_sel = 0 >> tous les dates.
                id_date_sel = "n" affiche le date dont l'id est "n"
"""


@obj_mon_application.route("/dates_afficher/<string:order_by>/<int:id_date_sel>", methods=['GET', 'POST'])
def dates_afficher(order_by, id_date_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion dates ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestiondates {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_date_sel == 0:
                    strsql_dates_afficher = """SELECT * FROM t_date ORDER BY id_date ASC"""
                    mc_afficher.execute(strsql_dates_afficher)
                elif order_by == "ASC":
                    # C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
                    # la commande MySql classique est "SELECT * FROM t_date"
                    # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
                    # donc, je précise les champs à afficher
                    # Constitution d'un dictionnaire pour associer l'id du date sélectionné avec un nom de variable
                    valeur_id_date_selected_dictionnaire = {"value_id_date_selected": id_date_sel}
                    strsql_dates_afficher = """SELECT * FROM t_date WHERE id_date = %(value_id_date_selected)s"""

                    mc_afficher.execute(strsql_dates_afficher, valeur_id_date_selected_dictionnaire)
                else:
                    strsql_dates_afficher = """SELECT * FROM t_date ORDER BY id_date DESC"""

                    mc_afficher.execute(strsql_dates_afficher)

                data_dates = mc_afficher.fetchall()

                print("data_dates ", data_dates, " Type : ", type(data_dates))

                # Différencier les messages si la table est vide.
                if not data_dates and id_date_sel == 0:
                    flash("""La table "t_date" est vide. !!""", "warning")
                elif not data_dates and id_date_sel > 0:
                    # Si l'utilisateur change l'id_date dans l'URL et que le date n'existe pas,
                    flash(f"Le date demandé n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_date" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données date affichées !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. dates_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} dates_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("date/date_afficher.html", data=data_dates)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /dates_ajouter
    
    Test : ex : http://127.0.0.1:5005/dates_ajouter
    
    Paramètres : sans
    
    But : Ajouter un date pour un film
    
    Remarque :  Dans le champ "name_date_html" du formulaire "dates/dates_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/dates_ajouter", methods=['GET', 'POST'])
def dates_ajouter():
    form = FormWTFAjouterdates()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion dates ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestiondates {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                date_wtf_value = form.date_wtf.data
                date_floraison_value = form.date_floraison_wtf.data
                date_note_value = form.info_date_wtf.data

                dico_date = {"value_date": date_wtf_value, "value_floraion": date_floraison_value, "value_note": date_note_value}
                strsql_insert_date = """INSERT INTO t_date (plantation_date, floraison_date, info_date) VALUES (%(value_date)s, %(value_floraion)s, %(value_note)s)"""

                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_date, dico_date)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('dates_afficher', order_by='DESC', id_date_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_date_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_date_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion dates CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("date/date_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /date_update
    
    Test : ex cliquer sur le menu "dates" puis cliquer sur le bouton "EDIT" d'un "date"
    
    Paramètres : sans
    
    But : Editer(update) un date qui a été sélectionné dans le formulaire "date_afficher.html"
    
    Remarque :  Dans le champ "nom_date_update_wtf" du formulaire "dates/date_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/date_update", methods=['GET', 'POST'])
def date_update():

    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_date"
    id_date_update = request.values['id_plat_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatedate()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "date_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            date_update = form_update.date_update_wtf.data
            date_floraison_update_value = form_update.date_floraison_update_wtf.data
            date_info_date = form_update.info_date_update_wtf.data

            dico_update = {"value_date": date_update, "value_id": id_date_update, "value_floraison": date_floraison_update_value, "value_info": date_info_date}
            str_sql_update_intituledate = """UPDATE t_date SET plantation_date = %(value_date)s, floraison_date = %(value_floraison)s, info_date = %(value_info)s WHERE id_date = %(value_id)s"""

            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intituledate, dico_update)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_date_update"
            return redirect(url_for('dates_afficher', order_by="ASC", id_date_sel=id_date_update))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_date" et "nom_date" de la "t_date"
            str_sql_id_date = "SELECT * FROM t_date WHERE id_date = %(value_id_date)s"
            valeur_select_dictionnaire = {"value_id_date": id_date_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_id_date, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom date" pour l'UPDATE
            data_nom_date = mybd_curseur.fetchone()
            print("data_nom_date ", data_nom_date, " type ", type(data_nom_date), " date ",
                  data_nom_date["plantation_date"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "date_update_wtf.html"
            form_update.date_update_wtf.data = data_nom_date["plantation_date"]
            form_update.date_floraison_update_wtf.data = data_nom_date["floraison_date"]

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans date_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans date_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans date_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans date_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("date/date_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /date_delete
    
    Test : ex. cliquer sur le menu "dates" puis cliquer sur le bouton "DELETE" d'un "date"
    
    Paramètres : sans
    
    But : Effacer(delete) un date qui a été sélectionné dans le formulaire "date_afficher.html"
    
    Remarque :  Dans le champ "nom_date_delete_wtf" du formulaire "dates/date_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/date_delete", methods=['GET', 'POST'])
def date_delete():
    data_films_attribue_date_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_date"
    id_date_delete = request.values['id_plat_btn_delete_html']

    # Objet formulaire pour effacer le date sélectionné.
    form_delete = FormWTFDeletedate()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("dates_afficher", order_by="ASC", id_date_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "dates/date_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_films_attribue_date_delete = session['data_films_attribue_date_delete']
                print("data_films_attribue_date_delete ", data_films_attribue_date_delete)

                flash(f"Effacer le date de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer date" qui va irrémédiablement EFFACER le date
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_date": id_date_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_films_date = """DELETE FROM t_date_plante WHERE fk_date = %(value_id_date)s"""
                str_sql_delete_iddate = """DELETE FROM t_date WHERE id_date = %(value_id_date)s"""
                # Manière brutale d'effacer d'abord la "fk_date", même si elle n'existe pas dans la "t_date_date"
                # Ensuite on peut effacer le date vu qu'il n'est plus "lié" (INNODB) dans la "t_date_date"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_films_date, valeur_delete_dictionnaire)
                    mconn_bd.mabd_execute(str_sql_delete_iddate, valeur_delete_dictionnaire)

                flash(f"date définitivement effacé !!", "success")
                print(f"date définitivement effacé !!")

                # afficher les données
                return redirect(url_for('dates_afficher', order_by="ASC", id_date_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_date": id_date_delete}
            print(id_date_delete, type(id_date_delete))

            # Requête qui affiche tous les films qui ont le date que l'utilisateur veut effacer
            str_sql_dates_films_delete = """SELECT nom_plante, plantation_date FROM t_date_plante
                                            INNER JOIN t_date ON t_date.id_date = t_date_plante.fk_date
                                            INNER JOIN t_plante ON t_plante.id_plante = t_date_plante.fk_plante
                                             WHERE fk_date = %(value_id_date)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_dates_films_delete, valeur_select_dictionnaire)
            data_films_attribue_date_delete = mybd_curseur.fetchall()
            print("data_films_attribue_date_delete...", data_films_attribue_date_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "dates/date_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_films_attribue_date_delete'] = data_films_attribue_date_delete

            # Opération sur la BD pour récupérer "id_date" et "nom_date" de la "t_date"
            str_sql_id_date = "SELECT id_date, plantation_date, floraison_date, info_date FROM t_date WHERE id_date = %(value_id_date)s"

            mybd_curseur.execute(str_sql_id_date, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom date" pour l'action DELETE
            data_nom_date = mybd_curseur.fetchone()
            print("data_nom_date ", data_nom_date, " type ", type(data_nom_date), " date ",
                  data_nom_date["plantation_date"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "date_delete_wtf.html"
            form_delete.nom_genre_delete_wtf.data = data_nom_date["plantation_date"]

            # Le bouton pour l'action "DELETE" dans le form. "date_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans date_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans date_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans date_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans date_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("date/date_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del,
                           data_films_associes=data_films_attribue_date_delete)
