"""
    Fichier : gestion_date_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.fields.html5 import DateField
from wtforms.validators import Length, Regexp


class FormWTFAjouterdates(FlaskForm):
    """
        Dans le formulaire "date_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    date_wtf = DateField("Date", format="%Y-%m-%d")

    date_floraison_wtf = DateField("Floraison", format="%Y-%m-%d")

    info_date_regex = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    info_date_wtf = StringField("Donner des informations ", validators=[Length(min=2, max=250, message="min 2 max 250"),
                                                                Regexp(info_date_regex,
                                                                       message="Pas de chiffres, de caractères "
                                                                               "spéciaux, "
                                                                               "d'espace à double, de double "
                                                                               "apostrophe, de double trait union")
                                                                ])

    submit = SubmitField("Enregistrer date")


class FormWTFUpdatedate (FlaskForm):
    """
        Dans le formulaire "date_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    date_update_wtf = DateField("Date", format="%Y-%m-%d")

    date_floraison_update_wtf = DateField("Floraison", format="%Y-%m-%d")

    info_date_regex = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    info_date_update_wtf = StringField("Donner des informations ", validators=[Length(min=2, max=250, message="min 2 max 250"),
                                                                Regexp(info_date_regex,
                                                                       message="Pas de chiffres, de caractères "
                                                                               "spéciaux, "
                                                                               "d'espace à double, de double "
                                                                               "apostrophe, de double trait union")
                                                                ])

    submit = SubmitField("Update date")


class FormWTFDeletedate(FlaskForm):
    """
        Dans le formulaire "date_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_plante".
    """
    nom_genre_delete_wtf = StringField("Effacer cette date")
    submit_btn_del = SubmitField("Effacer date")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
