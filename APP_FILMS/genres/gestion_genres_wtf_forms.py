"""
    Fichier : gestion_date_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "date_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_genre_regexp = "[a-zéèàêA-Z0-9_ ]"
    nom_plante = StringField("Clavioter le genre ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_genre_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    famille_plante = StringField("Famille ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                         Regexp(nom_genre_regexp,
                                                                message="Pas de chiffres, de caractères "
                                                                        "spéciaux, "
                                                                        "d'espace à double, de double "
                                                                        "apostrophe, de double trait union")
                                                         ])

    position_plante = StringField("Position ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                          Regexp(nom_genre_regexp,
                                                                 message="Pas de chiffres, de caractères "
                                                                         "spéciaux, "
                                                                         "d'espace à double, de double "
                                                                         "apostrophe, de double trait union")
                                                          ])

    note_plate = StringField("Note ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                          Regexp(nom_genre_regexp,
                                                                 message="Pas de chiffres, de caractères "
                                                                         "spéciaux, "
                                                                         "d'espace à double, de double "
                                                                         "apostrophe, de double trait union")
                                                          ])
    submit = SubmitField("Enregistrer plante")


class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "date_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_genre_update_regexp = ".+"
    nom_plante = StringField("Ecrire une note ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                Regexp(nom_genre_update_regexp,
                                                                       message="Pas de chiffres, de caractères "
                                                                               "spéciaux, "
                                                                               "d'espace à double, de double "
                                                                               "apostrophe, de double trait union")
                                                                ])
    famille_plante = StringField("Famille ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                         Regexp(nom_genre_update_regexp,
                                                                message="Pas de chiffres, de caractères "
                                                                        "spéciaux, "
                                                                        "d'espace à double, de double "
                                                                        "apostrophe, de double trait union")
                                                         ])

    position_plante = StringField("Position ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                           Regexp(nom_genre_update_regexp,
                                                                  message="Pas de chiffres, de caractères "
                                                                          "spéciaux, "
                                                                          "d'espace à double, de double "
                                                                          "apostrophe, de double trait union")
                                                           ])

    note_plante_regexp = "^\D+$"
    note_plate = StringField("Note ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                  Regexp(nom_genre_update_regexp,
                                                         message="Pas de chiffres, de caractères "
                                                                 "spéciaux, "
                                                                 "d'espace à double, de double "
                                                                 "apostrophe, de double trait union")
                                                  ])
    submit = SubmitField("Update plante")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "date_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_plante".
    """
    nom_genre_delete_wtf = StringField("Effacer ce genre")
    submit_btn_del = SubmitField("Effacer genre")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
